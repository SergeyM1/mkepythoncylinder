from pprint import pprint
from force_vector_forming import forces_vector_forming


def element_rigidity_matrix():
    pass


wall_thickness_of_cylinder = 0.1
radius_of_cylinder = 10
inner_radius_of_cylinder = radius_of_cylinder - wall_thickness_of_cylinder
length_of_cylinder = 10
number_of_nodes_through_the_thickness = 2
number_of_nodes_along_the_length = 2
number_of_nodes_along_the_circle = 2
poisson_ratio = 0.333
young_modulus = 627700
internal_pressure = 1.5
kti = 3

total_number_of_nodes = (number_of_nodes_along_the_length *
                         number_of_nodes_along_the_circle *
                         number_of_nodes_through_the_thickness)
total_number_of_elements = ((number_of_nodes_along_the_length - 1) *
                            (number_of_nodes_along_the_circle - 1) *
                            (number_of_nodes_through_the_thickness - 1))
step_of_changing_coordinates_along_the_circle = (
        0.5 * 3.14159 / (number_of_nodes_along_the_circle - 1))
step_of_changing_coordinates_along_the_thickness = (
        wall_thickness_of_cylinder /
        (number_of_nodes_through_the_thickness - 1))
step_of_changing_coordinates_along_the_length = (
        length_of_cylinder /
        (number_of_nodes_along_the_length - 1))
coordinates_along_length = [0] * (total_number_of_nodes + 1)
coordinates_along_circle = [0] * (total_number_of_nodes + 1)
coordinates_through_thickness = [0] * (total_number_of_nodes + 1)
current_node_number = total_number_of_nodes + 1
for i in range(1, number_of_nodes_along_the_length + 1, 1):
    for j in range(1, number_of_nodes_along_the_circle + 1, 1):
        for k in range(1, number_of_nodes_through_the_thickness + 1, 1):
            current_node_number = current_node_number - 1
            coordinates_along_length[current_node_number] = (
                    (i - 1) * step_of_changing_coordinates_along_the_length)
            coordinates_along_circle[current_node_number] = (
                    (j - 1) * step_of_changing_coordinates_along_the_circle)
            coordinates_through_thickness[current_node_number] = (
                    (k - 1) * step_of_changing_coordinates_along_the_thickness)

matrix_of_index = [0] * (total_number_of_nodes + 1)
for i in range(total_number_of_nodes + 1):
    matrix_of_index[i] = [0] * 13
current_node_number = 0
for i in range(1, number_of_nodes_along_the_length + 1, 1):
    for j in range(1, number_of_nodes_along_the_circle + 1, 1):
        for k in range(1, number_of_nodes_through_the_thickness + 1, 1):
            current_node_number += 1
            if i == 1:
                # matrix_of_index[current_node_number][1] = 1
                matrix_of_index[current_node_number][2] = 1
                matrix_of_index[current_node_number][3] = 1
                matrix_of_index[current_node_number][4] = 1
                matrix_of_index[current_node_number][5] = 1
                matrix_of_index[current_node_number][6] = 1
                matrix_of_index[current_node_number][7] = 1
                matrix_of_index[current_node_number][8] = 1
                matrix_of_index[current_node_number][9] = 1
                matrix_of_index[current_node_number][10] = 1
                matrix_of_index[current_node_number][11] = 1
                matrix_of_index[current_node_number][12] = 1
                continue
            elif (j == 1) or (j == number_of_nodes_along_the_circle):
                matrix_of_index[current_node_number][1] = 1
                matrix_of_index[current_node_number][2] = 1
                matrix_of_index[current_node_number][3] = 1
                matrix_of_index[current_node_number][4] = 1
                # matrix_of_index[current_node_number][5] = 1
                matrix_of_index[current_node_number][6] = 1
                matrix_of_index[current_node_number][7] = 1
                matrix_of_index[current_node_number][8] = 1
                matrix_of_index[current_node_number][9] = 1
                matrix_of_index[current_node_number][10] = 1
                matrix_of_index[current_node_number][11] = 1
                matrix_of_index[current_node_number][12] = 1
                continue
            matrix_of_index[current_node_number][1] = 1
            matrix_of_index[current_node_number][2] = 1
            matrix_of_index[current_node_number][3] = 1
            matrix_of_index[current_node_number][4] = 1
            matrix_of_index[current_node_number][5] = 1
            matrix_of_index[current_node_number][6] = 1
            matrix_of_index[current_node_number][7] = 1
            matrix_of_index[current_node_number][8] = 1
            matrix_of_index[current_node_number][9] = 1
            matrix_of_index[current_node_number][10] = 1
            matrix_of_index[current_node_number][11] = 1
            matrix_of_index[current_node_number][12] = 1
# pprint(matrix_of_index)
counter = 0
for i in range(1, total_number_of_nodes + 1, 1):
    for j in range(1, 13):
        if matrix_of_index[i][j] == 1:
            counter += 1
            matrix_of_index[i][j] = counter
pprint(matrix_of_index)
global_numbers_of_nodes_in_element = [0] * (total_number_of_elements + 1)
for i in range(total_number_of_elements + 1):
    global_numbers_of_nodes_in_element[i] = [0] * 9
print(global_numbers_of_nodes_in_element)
current_node_number = 1
for i in range(1, number_of_nodes_along_the_length, 1):
    for j in range(1, number_of_nodes_along_the_circle, 1):
        for k in range(1, number_of_nodes_through_the_thickness, 1):
            global_numbers_of_nodes_in_element[current_node_number][1] = (
                    k + i * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle +
                    number_of_nodes_through_the_thickness +
                    (j - 1) * number_of_nodes_through_the_thickness
            )
            global_numbers_of_nodes_in_element[current_node_number][2] = (
                    k + number_of_nodes_through_the_thickness +
                    (j - 1) * number_of_nodes_through_the_thickness +
                    (i - 1) * number_of_nodes_through_the_thickness *
                    number_of_nodes_through_the_thickness
            )
            global_numbers_of_nodes_in_element[current_node_number][3] = (
                    k + (j - 1) * number_of_nodes_through_the_thickness +
                    (i - 1) * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle
            )
            global_numbers_of_nodes_in_element[current_node_number][4] = (
                    k + i * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle +
                    (j - 1) * number_of_nodes_through_the_thickness
            )
            global_numbers_of_nodes_in_element[current_node_number][5] = (
                    k + i * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle +
                    number_of_nodes_through_the_thickness + 1 +
                    (j - 1) * number_of_nodes_through_the_thickness
            )
            global_numbers_of_nodes_in_element[current_node_number][6] = (
                    k + number_of_nodes_through_the_thickness + 1 +
                    (j - 1) * number_of_nodes_through_the_thickness +
                    (i - 1) * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle
            )
            global_numbers_of_nodes_in_element[current_node_number][7] = (
                    k + 1 + (j - 1) * number_of_nodes_through_the_thickness +
                    (i - 1) * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle
            )
            global_numbers_of_nodes_in_element[current_node_number][8] = (
                    k + i * number_of_nodes_through_the_thickness *
                    number_of_nodes_along_the_circle + 1 + (j - 1) *
                    number_of_nodes_through_the_thickness
            )
            current_node_number += 1
razn = 0
razn_1 = 0
pprint(global_numbers_of_nodes_in_element)
for i in range(1, total_number_of_elements + 1):
    if razn > razn_1:
        razn_1 = razn
    max_n = -100000000
    min_n = 100000000
    for j in range(1, 9):
        if global_numbers_of_nodes_in_element[i][j] > max_n:
            max_n = global_numbers_of_nodes_in_element[i][j]
        if global_numbers_of_nodes_in_element[i][j] < min_n:
            min_n = global_numbers_of_nodes_in_element[i][j]
    razn = max_n - min_n
print('razn =', razn)
iil = razn * 12 + 2
print('iil =', iil)
k = 0
nq = [0] * 9 * 13
for i in range(1, 9):
    for j in range(1, 13):
        k += 1
        nq[k] = i + 8 * (j - 1)
m12 = [0.0] * 2001
for i in range(1, 2001):
    m12[i] = [0.0] * 2001
nh = [0] * 97
# print(m12)
current_element_number = 0
ggme, fg = [0.0] * 97, [0.0] * 97
for i in range(1, 97):
    ggme[i] = [0.0] * 97
for i in range(1, number_of_nodes_along_the_length):
    for j in range(1, number_of_nodes_along_the_circle):
        for k in range(1, number_of_nodes_through_the_thickness):
            current_element_number += 1
            for ii in range(1, 13):
                nh[ii] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][1]][ii]
                nh[ii + 12] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][2]][ii]
                nh[ii + 24] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][3]][ii]
                nh[ii + 36] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][4]][ii]
                nh[ii + 48] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][5]][ii]
                nh[ii + 60] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][6]][ii]
                nh[ii + 72] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][7]][ii]
                nh[ii + 84] = matrix_of_index[global_numbers_of_nodes_in_element[current_element_number][8]][ii]
                if j == 1:
                    forces_vector_forming(current_element_number=current_element_number, kti=kti,
                                          coordinates_along_length=coordinates_along_length,
                                          coordinates_along_circle=coordinates_along_circle,
                                          coordinates_through_thickness=coordinates_through_thickness,
                                          internal_pressure=internal_pressure, radius_of_cylinder=radius_of_cylinder)
            element_rigidity_matrix()
            max_n = - 10000000000
            for iw in range(1, 97):
                l1 = nq[iw]
                ia = nh[iw]
                if ia == 0:
                    continue
                for iu in range(1, 97):
                    if nh[iu] < nh[iw]:
                        continue
                    if ia > max_n:
                        max_n = ia
                    ib = nh[iu] - ia + 1
                    ka = nq[iu]
                    m12[ia][ib] = m12[ia][ib] + ggme[l1][ka]
                m12[ia][iil] = m12[ia][iil] + fg[l1]

# TODO Найти и скачать модуль пайтон для решения линейных уравнений
