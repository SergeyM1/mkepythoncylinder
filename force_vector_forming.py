import numpy as np
def forces_vector_forming(current_element_number, kti, coordinates_along_length, coordinates_along_circle,
                          coordinates_through_thickness, internal_pressure, radius_of_cylinder,
                          global_numbers_of_nodes_in_element):
    def derivative(a, b, nodes_coordinates):
        o = [0.0] * 3
        s = [0.0] * 3
        s[1] = (-(1 - b) * coordinates_along_length[nodes_coordinates[1]] / 4
                + (1 - b) * coordinates_along_length[nodes_coordinates[2]] / 4
                + (1 + b) * coordinates_along_length[nodes_coordinates[3]] / 4
                - (1 + b) * coordinates_along_length[nodes_coordinates[4]] / 4)
        s[2] = (-(1 - a) * coordinates_along_length[nodes_coordinates[1]] / 4
                - (1 + a) * coordinates_along_length[nodes_coordinates[2]] / 4
                + (1 + a) * coordinates_along_length[nodes_coordinates[3]] / 4
                + (1 - a) * coordinates_along_length[nodes_coordinates[4]] / 4)
        o[1] = (-(1 - b) * coordinates_along_circle[nodes_coordinates[1]] / 4
                + (1 - b) * coordinates_along_circle[nodes_coordinates[2]] / 4
                + (1 + b) * coordinates_along_circle[nodes_coordinates[3]] / 4
                - (1 + b) * coordinates_along_circle[nodes_coordinates[4]] / 4)
        o[2] = (-(1 - a) * coordinates_along_circle[nodes_coordinates[1]] / 4
                + (1 - a) * coordinates_along_circle[nodes_coordinates[2]] / 4
                + (1 + a) * coordinates_along_circle[nodes_coordinates[3]] / 4
                - (1 + a) * coordinates_along_circle[nodes_coordinates[4]] / 4)
        dett = s[1] * o[2] - s[2] * o[1]
        return dett

    def func_tic(kti):
        tic = [0.0] * (kti + 1)
        if kti == 3:
            tic[1] = 0.774596669241
            tic[2] = 0
            tic[3] = 0.774596669241
        if kti == 4:
            tic[1] = - 0.8611363115
            tic[2] = - 0.3399810436
            tic[3] = 0.3399810436
            tic[4] = 0.8611363115
        if kti == 5:
            tic[1] = - 0.9061798459
            tic[2] = - 0.5384693101
            tic[3] = 0.0
            tic[4] = 0.5384693101
            tic[5] = 0.9061798459
        if kti == 7:
            tic[1] = - 0.9491079123
            tic[2] = - 0.7415311855
            tic[3] = - 0.4058451513
            tic[4] = 0.0
            tic[5] = 0.4058451513
            tic[6] = 0.7415311855
            tic[7] = 0.9491079123
        return tic

    def func_hm(kti):
        hm = [0.0] * (kti + 1)
        if kti == 3:
            hm[1] = 0.555555555555
            hm[2] = 0.888888888888
            hm[3] = 0.555555555555
        if kti == 4:
            hm[1] = 0.3478548451
            hm[2] = 0.6521451549
            hm[3] = 0.6521451549
            hm[4] = 0.3478548451
        if kti == 5:
            hm[1] = 0.2369268850
            hm[2] = 0.4786286704
            hm[3] = 0.5688888888
            hm[4] = 0.4786286704
            hm[5] = 0.2369268850
        if kti == 7:
            hm[1] = 0.1294849661
            hm[2] = 0.2797053914
            hm[3] = 0.3818300505
            hm[4] = 0.4179591836
            hm[5] = 0.3818300505
            hm[6] = 0.2797053914
            hm[7] = 0.1294849661
        return hm

    def polinom(i, j, k):
        def h(n, x):
            if n == 1:
                return 0.25 * (x ** 3 - 3 * x + 2)
            if n == 2:
                return - 0.25 * (x ** 3 - 3 * x - 2)
            if n == 3:
                return 0.25 * (x ** 3 - x ** 2 - x + 1)
            if n == 4:
                return 0.25 * (x ** 3 + x ** 2 - x - 1)

        p = [0.0] * 33
        p[1] = h(1, i) * h(1, j)
        p[2] = h(2, i) * h(1, j)
        p[3] = h(2, i) * h(2, j)
        p[4] = h(1, i) * h(2, j)
        p[5] = 0
        p[6] = 0
        p[7] = 0
        p[8] = 0
        p[9] = h(3, i) * h(1, j)
        p[10] = h(4, i) * h(1, j)
        p[11] = h(4, i) * h(2, j)
        p[12] = h(3, i) * h(2, j)
        p[13] = 0
        p[14] = 0
        p[15] = 0
        p[16] = 0
        p[17] = h(1, i) * h(3, j)
        p[18] = h(2, i) * h(3, j)
        p[19] = h(2, i) * h(4, j)
        p[20] = h(1, i) * h(4, j)
        p[21] = 0
        p[22] = 0
        p[23] = 0
        p[24] = 0
        p[25] = h(1, i) * h(1, j)
        p[26] = h(2, i) * h(1, j)
        p[27] = h(2, i) * h(2, j)
        p[28] = h(1, i) * h(2, j)
        p[29] = 0
        p[30] = 0
        p[31] = 0
        p[32] = 0
        return p

    tic = func_tic(kti)
    hm = func_hm(kti)
    node_coordinates = [0] * 9
    for i in range(1, 9):
        node_coordinates[i] = global_numbers_of_nodes_in_element[current_element_number][i]
    f = [0.0] * 97
    for i in range(1, kti + 1):
        for j in range(1, kti + 1):
            dett = derivative(tic[i], tic[j], node_coordinates)
            print(dett)
            p = polinom(tic[i], tic[j], 1)
            for i1 in range(1, 33):
                f[i1] = f[i1] + internal_pressure * p[i1] * abs(dett) * hm[i] * hm[j] * radius_of_cylinder
    return f
